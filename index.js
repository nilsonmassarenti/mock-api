const express = require('express');
const bodyParser = require('body-parser')
const app = express();
const port = process.env.PORT || 8080;
const jsonParser = bodyParser.json();
const urlencodedParser = bodyParser.urlencoded({ extended: false })

app.get('/', (req, res) => {
  res.send('Hello World!')
});

app.post('/v1/boleto/consultar', jsonParser, function (req, res) {
  if (req.body.conta === undefined || req.body.codigoBarras === undefined) {
    res.status(400).json({msg: "conta não encontrada"});
  } else {
    if ( eq.body.codigoBarras.substring(0, 1) === '8') {
      res.status(400).json({msg: "Esta API não processa concessionárias"});
    }
    res.status(200).json(require('./mock/boletoConsultar.json'));
  }

});

app.post('/v1/boleto/validar', jsonParser, function (req, res) {
  if (req.body.conta === undefined || req.body.validaBoleto === undefined || req.body.nsuConsulta === undefined) {
    res.status(400).json({msg: "conta não encontrada"});
  } else {
    const nsuConsulta = req.body.nsuConsulta;
    if (nsuConsulta !== 1) {
      res.status(404).json({msg: "Este NSU não existe."});
    }
    res.status(200).json(require('./mock/boletoValidar.json'));
  }
});

app.post('/v1/boleto/efetivar', jsonParser, function (req, res) {
  if (req.body.conta === undefined || req.body.pagamentoBoleto === undefined || req.body.nsuValidacao === undefined) {
    res.status(400).json({msg: "conta não encontrada"});
  } else {
    const nsuValidacao = req.body.nsuValidacao;
    if (nsuValidacao !== 2) {
      res.status(404).json({msg: "Este NSU não existe."});
    }
    res.status(200).json(require('./mock/boletoEfetivar.json'));
  }

});

app.listen(port, () => {
  console.log(`Mock at http://localhost:${port}`)
})
